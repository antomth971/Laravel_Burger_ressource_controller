@extends('presentation.app')
@extends('presentation.nav')
@section('contenu')
    @if (Session::has('message'))
        <div class="alert alert-success">
            {{ Session::get('message') }}
        </div>
    @endif
    <div class="container-fluid">
        <div class="d-flex justify-content-center mx-auto">


            @foreach ($produits as $item)
                <div class="card" style="width: 18rem;">
                    <img class="card-img-top" src="/Images/{{ $item->image }}" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title">{{ $item->nom }}</h5>
                        <p class="card-text">{{ $item->description }}</p>
                        <p class="card-text"><b>{{ $item->prix }}€</b></p>

                        <a href="{{ $item->id }}/edit" class="btn btn-warning">Modifier</a>
                        {!! Form::open(['action' => ['App\Http\Controllers\ProdController@destroy', $item->id]]) !!}
                        {{ method_field('DELETE') }}
                        {{ Form::submit('Supprimer', ['class' => 'btn btn-danger']) }}
                        {!! Form::close() !!}

                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
