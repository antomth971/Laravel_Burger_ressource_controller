@extends('presentation.app')
@extends('presentation.nav')

@section('contenu')
    <h1>Bienvenue sur la page Update</h1>
    {!! Form::open(['action' => ['App\Http\Controllers\ProdController@update', $produit->id], 'method' => 'POST', 'class' => 'container', 'files' => true]) !!}
    {{ method_field('PUT') }}
    {{ Form::label('', 'Nom') }}
    {{ Form::text('nom', $produit->nom, ['placeholder' => 'Nom du stagiaire', 'class' => 'form-control']) }}
    <br>
    {{ Form::label('', 'Prix') }}
    {{ Form::number('prix', $produit->prix, ['placeholder' => 'Prix du produit', 'class' => 'form-control']) }}
    <br>
    {{ Form::label('', 'Description') }}
    {{ Form::text('description', $produit->description, ['placeholder' => 'Description du produit','class' => 'form-control']) }}
    <br>
    {{ Form::label('', 'Image') }}
    {{ Form::file('image') }}
    <br>
    {{ Form::select('idCat', ['1' => 'Menu', '2' => 'Burger', '3' => 'Salade', '4' => 'Boisson', '5' => 'Dessert']) }}
    <br><br>
    {{ Form::submit('Modifier le produit', ['class' => 'btn btn-success']) }}
    {!! Form::close() !!}
@endsection
