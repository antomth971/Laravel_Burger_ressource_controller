@extends('presentation.app')
@extends('presentation.nav')



@section('contenu')
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li> {{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <center>
        <h1>Ajoutez un produit/menu</h1>
    </center>

    @if (Session::has('message'))
        <div class="alert alert-succes">
            {{ Session::get('message') }}
        </div>
    @endif
    {!! Form::open(['action' => 'App\Http\Controllers\ProdController@store', 'method' => 'POST', 'class' => 'container', 'files' => true]) !!}
    {{ Form::text('nom', '', ['placeholder' => 'Nom du produit', 'class' => 'form-control']) }}
    {{ Form::text('prix', '', ['placeholder' => 'Prix du produit', 'class' => 'form-control']) }}
    {{ Form::number('idCat', '', ['placeholder' => 'id de la catégorie', 'class' => 'form-control']) }}
    {{ Form::textarea('description', '', ['placeholder' => 'Description', 'max' => 255, 'class' => 'form-control']) }}

    {{ Form::file('image') }}

    {{ Form::submit('Ajouter', ['class' => 'form-control btn-secondary']) }}

    {!! Form::close() !!}
@endsection
