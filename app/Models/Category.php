<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Category
 * 
 * @property int $id
 * @property string $nomCat
 * 
 * @property Collection|Produit[] $produits
 *
 * @package App\Models
 */
class Category extends Model
{
	protected $table = 'categories';
	public $timestamps = false;

	protected $fillable = [
		'nomCat'
	];

	public function produits()
	{
		return $this->hasMany(Produit::class, 'idCat');
	}
}
