<?php

use App\Http\Controllers\ProdController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('produits', ProdController::class);

// Route::get('produits/{id}', [ProdController::class, 'show']);

// Route::get('produits/{id}/edit', [ProdController::class, 'edit']);

// Route::put('produits/{id}', [ProdController::class, 'update']);

// Route::delete('produits/{id}', [ProdController::class, 'destroy']);
